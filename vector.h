/*
Vector implementation for C.
Copyright (C) 2019 Joakim Vinteräng

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VECTOR_H
#define VECTOR_H

#ifndef VECTOR_BLOCK_SIZE
    #define VECTOR_BLOCK_SIZE 50
#endif

#ifndef ESUCCESS
    #define ESUCCESS 0
#endif

typedef struct Vector {
    void **data;
    size_t i;                   /* iterator index */
    size_t num_elements;
    size_t capacity;
    void* (*free_func)(void*);
} Vector;

Vector* vector_new();
int     vector_free(Vector **v);
int     vector_append(Vector *v, void *data);
Vector* vector_copy(Vector *orig);
size_t  vector_count(Vector *v);
size_t  vector_capacity(Vector *v);
int     vector_expand(Vector *v);
void*   vector_get(Vector *v, size_t i);
size_t  vector_index(Vector *v);
void*   vector_iterate(Vector *v);
int     vector_remove(Vector *v, size_t i);
void    vector_reset(Vector *v);
void    vector_set_free(Vector *v, void *free_func(void*));
int     vector_shrink(Vector *v);

#endif
