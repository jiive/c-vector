/*
 * Vector implementation for C.
 * Copyright (C) 2019 Joakim Vinteräng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
    Simple example to show common vector functions.
    Generates strings of random numbers and appends them to vector, then
    iterates over vector and prints the numbers.

    Compile with:
    cc example.c vector.c -o vector_test

    Example output:
    $ ./vector_text
    Element #0: 2058710298
    Element #1: 793268762
    Element #2: 1687804206
    Element #3: 246995710
    Element #4: 280783779
    Element #5: 81735466
    Element #6: 596661775
    Element #7: 1885146129
    Element #8: 775331403
    Element #9: 562324881
    Element 5 is 81735466
    One element removed, Vector v now contains 9 elements
    Vector v is now (null)
    $ _
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "vector.h"

/* Helper function */
static char* generate_number();

int
main()
{
    char *node = NULL;

    /* Create and initialize new vector
     * By default this allocates 50 blocks, this can be changed in vector.h
     * Here we pass free() as a function-pointer, this enables our vector to
     * automatically free all elements when we call vector_free() */
    Vector *v = vector_new(&free);

    /* Generate 10 random number as string, and append to vector */
    srand(time(NULL));
    for (int i = 0; i < 10; i++) {
        vector_append(v, generate_number());
    }

    /* Reset vector iterator to start from the beginning */
    vector_reset(v);
    /* Call to vector_iterate returns next pointer in vector */
    while ((node = vector_iterate(v))) {
        printf("Element #%zu: %s\n", vector_index(v)-1, node);
    }

    /* vector_get() retrieves an element at a specific index */
    printf("Element %d is %s\n", 5, vector_get(v, 5));

    /*  Remove a specific element (and free content, since free function was
     *  set on vector creation). */
    vector_remove(v, 2);

    /* Get current number of elements with vector_count() */
    printf("One element removed, Vector v now contains %zu elements\n", vector_count(v));

    /* Free our vector and all elements it contains */
    vector_free(&v);
    printf("Vector v is now %s\n", (char*)v);

    return 0;
}

static char*
generate_number()
{
    long value, len;
    char *str = NULL;

    value = rand();
    len = snprintf(NULL, 0, "%ld", value);
    str = calloc(1, len+1);
    snprintf(str, len+1, "%ld", value);

    return str;
}
