# Vector implementation written in C

License: Gnu GPL v3+
Documentation: Doxygen

## Example:
```c
    char *node = NULL;

    /* Create and initialize new vector
     * Automatically call free() on all objects on desctruction */
    Vector *v = vector_new(&free);

    /* ... */
    for (int i = 0; i < 10; i++) {
        vector_append(v, generate_number());
    }

    /* Iterate over objects */
    vector_reset(v);
    while ((node = vector_iterate(v))) {
        printf("Element #%zu: %s\n", vector_index(v)-1, node);
    }

    printf("Element %d is %s\n", 5, vector_get(v, 5));

    vector_free(&v);
```

See example.c for full example.
