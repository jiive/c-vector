#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "vector.h"

/**
 *  @brief Initializes Vector to VECTOR_BLOCK_SIZE
 *
 *  @param v_destroy: Function pointer to deallocate elements on
 *                    vector_remove() or vector_free(). Elements are not
 *                    not deallocated and must be manually done
 *                    from calling code if v_destroy is set to NULL.
 *  @return Pointer to newly created Vector, NULL on error
 */
Vector*
vector_new(void* v_destroy(void*))
{
    Vector *new_v = calloc(1, sizeof(Vector));
    if (!new_v) return NULL;

    new_v->data = calloc(VECTOR_BLOCK_SIZE, sizeof(void*));
    if (!new_v->data) {
        free(new_v);
        return NULL;
    }

    if (v_destroy) {
        new_v->free_func = v_destroy;
    }

    new_v->i = 0;
    new_v->num_elements = 0;
    new_v->capacity = VECTOR_BLOCK_SIZE;

    return new_v;
}

/**
 *  @brief Calls function f for each element in Vector v
 *  NOTE: If free function was provided on vector_new, this will be called
 *        on each element in vector. If no function was provided, elements
 *        will have to be freed manually from calling code.
 *
 *  @param v: Vector
 *  @return ESUCCESS on success, EINVAL on invalid v
 */
int
vector_free(Vector **v)
{
    if (!v) return -EINVAL;
    size_t i = 0;

    if ((*v)->free_func) {
        while (i < (*v)->num_elements) {
            (*v)->free_func((*v)->data[i]);
            (*v)->data[i] = NULL;
            i++;
        }
    }

    (*v)->num_elements = 0;
    (*v)->capacity = 0;

    free((*v)->data);
    (*v)->data = NULL;

    (*v)->free_func = NULL;

    free((*v));
    (*v) = NULL;

    return ESUCCESS;
}

/**
 *  @brief Appends pointer to end of Vector
 *
 *  @param v: Vector
 *  @param data: pointer to be added to Vector v
 *  @return ESUCCESS on success, ENOMEM on failure to extend vector,
 *  EINVAL on invalid v
 */
int
vector_append(Vector *v, void *data)
{
    if (!v) return -EINVAL;

    if (v->num_elements == v->capacity)
        if (vector_expand(v) != ESUCCESS)
            return -ENOMEM;

    v->data[v->num_elements] = data;
    v->num_elements++;

    return ESUCCESS;
}

/**
 *  @brief Copies a Vector
 *
 *  NOTE: This might be expensive for large vectors
 *  NOTE2: Keep in mind that this copies pointers! This function does not copy
 *         the element destroy function provided with the original vector.
 *         Doing this will cause double free!
 *
 *  @param orig: Vector
 *  @return Pointer to new vector, NULL on error
 */
Vector*
vector_copy(Vector *orig)
{
    Vector *new_v = vector_new(NULL);
    Vector *node = NULL;

    if (!new_v) return NULL;

    vector_reset(orig);
    while ((node = vector_iterate(orig))) {
        vector_append(new_v, node);
    }

    if (new_v->num_elements != vector_count(orig)) {
        vector_free(&new_v);
        return NULL;
    }

    if (new_v->num_elements == vector_capacity(orig)) {
        vector_shrink(new_v);
    }

    return new_v;
}

/**
 *  @brief Returns number of elements in Vector
 *
 *  @param v: Vector
 *  @return Number of elements in Vector v
 */
size_t
vector_count(Vector *v)
{
    if (!v) return 0;

    return v->num_elements;
}

/**
 *  @brief Increases capacity of Vector v by VECTOR_BLOCK_SIZE elements
 *
 *  @param v: Vector
 *  @return ESUCCESS on success, ENOMEM in failure to reallocate structure
 */
int
vector_expand(Vector *v)
{
    if (!v) return -EINVAL;

    v->data = realloc(v->data, (VECTOR_BLOCK_SIZE + v->num_elements) * sizeof(void*));
    if (!v->data) {
        v->num_elements = 0;
        v->capacity = 0;
        return -ENOMEM;
    }

    memset(&v->data[v->num_elements], '\0', VECTOR_BLOCK_SIZE-1);
    v->capacity = v->num_elements + VECTOR_BLOCK_SIZE;

    return ESUCCESS;
}

/**
 *  @brief Returns pointer to element number i in Vector v
 *
 *  @param v: Vector
 *  @param i: index
 *  @return void pointer to element
 */
void*
vector_get(Vector* v, size_t i)
{
    if (v && (v->num_elements > 0) && (i < v->num_elements))
        return v->data[i];

    return NULL;
}
/**
 *  @brief Returns the current index(element number) in iteration.
 *
 *  NOTE: The index always points to the next element in the iteration.
 *
 *  @param v: Vector
 *  @return size_t integer index value.
 */
size_t
vector_index(Vector *v)
{
    return v->i;
}

/**
 *  @brief Returns the next object in iteration
 *
 *  NOTE: This might return a NULL if Vector v contains NULL pointers
 *        indicating end of iteration in a while-loop for example
 *
 *  @param v: Vector
 *  @return Pointer to next data-element in Vector, NULL after last element
 */
void*
vector_iterate(Vector *v)
{
    if (v->i < v->num_elements) {
        void *ptr = vector_get(v, v->i);
        v->i++;
        return ptr;
    }

    return NULL;
}

/**
 *  @brief Returns total storage capacity in Vector
 *
 *  @param v: Vector
 *  @return Capacity in number of elements
 */
size_t
vector_capacity(Vector *v)
{
    if (!v) return 0;

    return v->capacity;
}

/**
 *  @brief Removes element i from Vector v
 *
 *  NOTE:   Data pointer at element i will only be freed if a v_destroy
 *          was provided to vector_new, if no function-pointer was provided,
 *          element i will have to be freed manually from calling code.
 *  NOTE2:  All element pointers at preceding elements will be moved up one
 *          slot. This can be time consuming on very large vectors.
 *
 *  @param v: Vector
 *  @param i: index of element to be removed
 *  @return ESUCCESS on success, EINVAL on invalid index i
 */
int
vector_remove(Vector *v, size_t i)
{
    if (!v || i >= v->num_elements) return -EINVAL;

    if (v->free_func) {
        v->free_func(v->data[i]);
    }

    v->num_elements--;

    while (i < v->num_elements) {
        v->data[i] = v->data[i+1];
        i++;
    }

    return ESUCCESS;
}

/**
 *  @brief Resets iterator counter of v
 *
 *  @param v: Vector
 *  @return void
 */
void
vector_reset(Vector *v)
{
    v->i = 0;
}

/**
 *  @brief Sets functionspointer to be used on each element by vector_remove
 *  and vector_free
 *
 *  @param v: Vector
 *  @param free_func: Function pointer to void function taking one argument of
 *                    element type
 *  @return void
 */
void
vector_set_free(Vector *v, void *free_func(void*))
{
    v->free_func = free_func;
}

/**
 *  @brief Deallocates unused memory
 *
 *  @param v: Vector
 *  @return void
 */
int
vector_shrink(Vector *v)
{
    if (!v) return -EINVAL;

    if (v->num_elements > 0 && v->num_elements < v->capacity) {
        v->data = realloc(v->data, v->num_elements * sizeof(void*));
        if (!v->data) {
            v->num_elements = 0;
            v->capacity = 0;
            return -ENOMEM;
        }

        v->capacity = v->num_elements;
    }

    return ESUCCESS;
}
